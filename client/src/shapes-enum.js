const ShapesEnum = {
  SINGLE_CUT: 'scut',
  S_TYPE: 'stype',
  T_TYPE: 'ttype',
  V_TYPE: 'vtype',
  ALL: 'all'
};

export default ShapesEnum;