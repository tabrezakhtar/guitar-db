import React from 'react';
import { shallow } from 'enzyme';
import Shapes from './shapes';
import ShapesEnum from '../shapes-enum';
import FilterTypes from '../filter-types';
import * as filterActions from '../actions/filtersActions';
import {updateFilter} from "../actions/filtersActions";

afterEach( () => {
  jest.restoreAllMocks()
});

describe('when the component is loads', () => {
  test('it renders the guitar shapes', () => {
    const wrapper = shallow(<Shapes.WrappedComponent />);
  
    expect(wrapper.containsMatchingElement(
      <React.Fragment>
        <span className="sidebar__category">
          view by shape
        </span>

        <div className="sidebar__shapes">

          <input defaultChecked type="radio" id="radioAll" name="guitarsShapes" value={ShapesEnum.ALL}/>
          <label className="labelAll" for="radioAll">
            <div>All</div>
          </label> 

          <input type="radio" id="radioSingleCut" name="guitarsShapes" value={ShapesEnum.SINGLE_CUT}/>
          <label for="radioSingleCut">
            <img src="/singlecut_outline.png" alt="Single Cut Guitars"/>
          </label>

          <input type="radio" id="radioSType" name="guitarsShapes" value={ShapesEnum.S_TYPE}/>
          <label for="radioSType">
            <img src="/stype_outline.png" alt="S-Type Guitars"/>
          </label>

          <input type="radio" id="radioTType" name="guitarsShapes" value={ShapesEnum.T_TYPE}/>
          <label for="radioTType">
            <img src="/ttype_outline.png" alt="T-Type Guitars"/>
          </label> 

          <input type="radio" id="radioVType" name="guitarsShapes"value={ShapesEnum.V_TYPE}/>
          <label for="radioVType">
            <img src="/v_outline.png" alt="V-Type Guitars"/>
          </label> 
        </div>
      </React.Fragment>
    )).toEqual(true);
  });
});

describe('when the shapes are clicked', () => {
  test('the ALL button is clicked', () => {
    const dispatchStub = jest.fn();
    const updateFilterSpy = jest.spyOn(filterActions, 'updateFilter');
    const wrapper = shallow(<Shapes.WrappedComponent dispatch={dispatchStub}/>);
    wrapper.find('#radioAll').simulate('click');
    expect(dispatchStub).toBeCalledWith(expect.any(Function));
    expect(updateFilterSpy).toBeCalledWith(filterActions.RESET_FILTER, FilterTypes.SHAPE);
  });

  test('the SINGLE CUT button is clicked', () => {
    const dispatchStub = jest.fn();
    const updateFilterSpy = jest.spyOn(filterActions, 'updateFilter');
    const wrapper = shallow(<Shapes.WrappedComponent dispatch={dispatchStub}/>);
    wrapper.find('#radioSingleCut').simulate('click');
    expect(dispatchStub).toBeCalledWith(expect.any(Function));
    expect(updateFilterSpy).toBeCalledWith(filterActions.ADD_SHAPE, ShapesEnum.SINGLE_CUT);
  });

  test('the STYPE button is clicked', () => {
    const dispatchStub = jest.fn();
    const updateFilterSpy = jest.spyOn(filterActions, 'updateFilter');
    const wrapper = shallow(<Shapes.WrappedComponent dispatch={dispatchStub}/>);
    wrapper.find('#radioSType').simulate('click');
    expect(dispatchStub).toBeCalledWith(expect.any(Function));
    expect(updateFilterSpy).toBeCalledWith(filterActions.ADD_SHAPE, ShapesEnum.S_TYPE);
  });

  test('the TTYPE button is clicked', () => {
    const dispatchStub = jest.fn();
    const updateFilterSpy = jest.spyOn(filterActions, 'updateFilter');
    const wrapper = shallow(<Shapes.WrappedComponent dispatch={dispatchStub}/>);
    wrapper.find('#radioTType').simulate('click');
    expect(dispatchStub).toBeCalledWith(expect.any(Function));
    expect(updateFilterSpy).toBeCalledWith(filterActions.ADD_SHAPE, ShapesEnum.T_TYPE);
  });

  test('the VTYPE button is clicked', () => {
    const dispatchStub = jest.fn();
    const updateFilterSpy = jest.spyOn(filterActions, 'updateFilter');
    const wrapper = shallow(<Shapes.WrappedComponent dispatch={dispatchStub}/>);
    wrapper.find('#radioVType').simulate('click');
    expect(dispatchStub).toBeCalledWith(expect.any(Function));
    expect(updateFilterSpy).toBeCalledWith(filterActions.ADD_SHAPE, ShapesEnum.V_TYPE);
  });
});