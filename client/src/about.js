import React from 'react';

function About() {
  return <div className="tile about">
      <p className="description">
        A full stack demo of a guitar shopping site, with a fast easy to use filter.
      </p>

      <p className="description">The API is built with Node and MongoDB and the front end is React/Redux with handwritten SASS.</p>

    </div>
}

export default About;