const FilterTypes = {
  SHAPE: 'shape',
  BRAND: 'brand',
  PICKUPS: 'pickups',
  TREMOLO: 'tremolo',
  FRETBOARD: 'fretboard',
  ALL: 'all'
};

export default FilterTypes;