import React, { useEffect, useState } from 'react';
import { BrowserRouter, NavLink, Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { fetchGuitars } from './actions/guitarsActions'
import { fetchGuitarsByFilter } from './actions/filtersActions'
import ImageGallery from 'react-image-gallery';
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import SideBar from './sidebar';
import Tiles from './rhs';
import About from './about';
import Menu from './menu';
import Hamburger from './menu/hamburger.js';

function App({dispatch, guitarList, filters, loading, hasErrors}) {
  useEffect(() => {
    if (isEmpty(filters)) {
      dispatch(fetchGuitars())
    } else {
      dispatch(fetchGuitarsByFilter(filters))
    }
  }, [dispatch, filters]);

  const [open, setOpen] = useState(false);
  const [image, setImage] = useState();

  const onOpenModal = (imageUrl) => {
    setImage([
      {
        original: imageUrl,
        thumbnail: imageUrl,
      },
      {
        original: 'images/strat1.jpeg',
        thumbnail: 'images/strat1.jpeg',
      },
      {
        original: 'images/strat2.jpeg',
        thumbnail: 'images/strat2.jpeg',
      },
      {
        original: 'images/strat3.jpeg',
        thumbnail: 'images/strat3.jpeg',
      },
      {
        original: 'images/strat4.jpeg',
        thumbnail: 'images/strat4.jpeg',
      },
    ]);

    setOpen(true);
  };

  const onCloseModal = () => {
    setOpen(false);
  };

  return (
    <BrowserRouter>
      <header className="header">
        <div className="header__logo-box">
          <NavLink to='/'><span>Guitar Explorer<span role="img" aria-label="logo fire">🔥</span></span></NavLink>
        </div>

        <Hamburger />
        <Menu />

      </header>

      <div className="container">
        
        <SideBar />

        <div className="rhs">
        <Switch>
          <Route exact path='/'>
            <Tiles guitars={guitarList} onOpenModal={onOpenModal} loading={loading}/>
          </Route>
          
          <Route exact path='/about' component={About}/>
        </Switch>
          
        </div>
      </div>

      <Modal open={open} onClose={onCloseModal}>
        <div className="popup">
          <div className="popup__content">
            <div className="popup__left">
              <ImageGallery items={image} showFullscreenButton={false} showPlayButton={false}/>
            </div>
            <div className="popup__right">
              <h3>Specs</h3>
              <ul>
                <li>Alder body with gloss finish</li>
                <li>Three Player Series single-coil Stratocaster pickups</li>
                <li>Modern C-shaped neck profile</li>
                <li>9.5"-radius fingerboard</li>
                <li>2-point tremolo bridge with bent-steel saddles</li>
              </ul>
            </div>
          </div>
        </div>
      </Modal>
    </BrowserRouter>
  );
}

const mapStateToProps = state => ({
  guitarList: state.guitars.guitars,
  loading: state.guitars.loading,
  filters: state.filters
});

// export default withRouter(connect(mapStateToProps)(App))
export default connect(mapStateToProps)(App)