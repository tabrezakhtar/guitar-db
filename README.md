# Guitar Explorer

<img src="https://gitlab.com/tabrezakhtar/guitar-db/-/raw/master/client/public/guitar-db.png" alt="Image of Guitar Explorer" width="600px"/>

A Full Stack Javascript app build with MongoDB, Node/Express, React and Sass.

To run the API:

`npm install`

`npm start`

The API will start on port 5000.

Open `localhost:5000` to run the api.

Change to the client directory and 
`npm install`

`npm start`

The client app will run on port 3000.

Open `localhost:3000` to run the app.